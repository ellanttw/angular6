export class blog {
  "id": number;
  "title": string;
  "desc": string;
  "image": string;
  "author": string;
  "date": string;
  "category": string
}
