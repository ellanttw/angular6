import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { CustomersListComponent } from './components/customers-list/customers-list.component';
import { CustomerDetailComponent } from './components/customer-detail/customer-detail.component';

const routes: Routes = [
  { path: '', redirectTo:'/post', pathMatch: 'full' },
  { path: 'post', component: CustomersListComponent },
  { path: 'post/:id', component: CustomerDetailComponent },
  { path: '**', redirectTo:'', pathMatch: 'full' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRouterModule {}
