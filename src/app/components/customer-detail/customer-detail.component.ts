import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CustomerService } from '../../services/customer.service';
import { blog } from '../../models/customer.modal';
import { Store, select } from '@ngrx/store';
import { AppState } from '../../models/app.state';
import { Location } from '@angular/common';
import { Observable } from 'rxjs'

@Component({
  selector: 'app-customer-detail',
  templateUrl: './customer-detail.component.html',
  styleUrls: ['./customer-detail.component.css']
})
export class CustomerDetailComponent implements OnInit {
  @Output() update = new EventEmitter<blog>();
  customers$:Observable<any>;
  private customerId: number;
  public post: blog;
  public posting : {};
  constructor(private route: ActivatedRoute,
    private customerService: CustomerService,
    private store: Store<AppState>,
    private location: Location
  ) { 
    // this.customers$ = this.store.select('applicationState') 
    this.posting = store.pipe(select('blog')); 
  }

  ngOnInit() {
    this.customerId = +this.route.snapshot.paramMap.get('id');
    this.getCustomer();
  }
  
  getCustomer(): void {
    this.customerService.getCustomer(this.customerId)
      .subscribe(customer => this.test(customer));
  }
  test(resp){
    this.post = resp.data[0]
  }
  goBack() {
    this.location.back();
  }

}
