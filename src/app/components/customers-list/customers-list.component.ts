import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { blog } from '../../models/customer.modal';
import { Store } from '@ngrx/store';
import { AppState } from '../../models/app.state';
import * as CustomerActions from '../../store/customer.actions';
import { Observable } from 'rxjs'

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent implements OnInit {
  customers$:Observable<any>;
  public posting: blog[];

  constructor(private store: Store<AppState>) {
    this.customers$ = this.store.select('applicationState');
  }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers() {
    this.store.dispatch(new CustomerActions.loadCustomersAction());
    this.customers$.subscribe((state:AppState) => this.posting = state.blog );  
    
  }

}
